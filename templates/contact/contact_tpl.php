<div class="title-main">
    <h1><?=$title_crumb?></h1>
    <p><?=$slogan['ten']?></p>
</div>
<div class="w-clear">
    <div class="center">
        <div class="div-lienhe">
            <div class="left-lienhe">
                <div class="text-ggmap-ct">
                    <p>GOOGLE MAP</p>
                </div>
                <div class="bottom-contact1"><?=htmlspecialchars_decode($optsetting['toado_iframe'])?></div>
            </div>
            <div class="right-lienhe">
                <div class="text-ggmap-ct">
                    <p>LIÊN HỆ VỚI CHÚNG TÔI</p>
                </div>
                <form class="form-contact1 validation-contact" novalidate method="post" action="" enctype="multipart/form-data">
                    <div class="row">
                        <div class="input-contact col-sm-6">
                            <input type="text" class="form-control" id="ten" name="ten" placeholder="Họ tên*" required />
                            <div class="img-lienhe1">
                                <img src="assets/img/img-lienhe1.png">
                            </div>
                            <div class="invalid-feedback"><?=vuilongnhaphoten?></div>
                        </div>
                        <div class="input-contact col-sm-6">
                            <input type="number" class="form-control" id="dienthoai" name="dienthoai" placeholder="Số điện thoại*" required />
                            <div class="img-lienhe1">
                                <img src="assets/img/img-lienhe2.png">
                            </div>
                            <div class="invalid-feedback"><?=vuilongnhapsodienthoai?></div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-contact col-sm-6">
                            <input type="text" class="form-control" id="diachi" name="diachi" placeholder="Địa chỉ*" required />
                            <div class="img-lienhe1">
                                <img src="assets/img/img-lienhe1.png">
                            </div>
                            <div class="invalid-feedback"><?=vuilongnhapdiachi?></div>
                        </div>
                        <div class="input-contact col-sm-6">
                            <input type="email" class="form-control" id="email" name="email" placeholder="Email*" required />
                            <div class="img-lienhe1">
                                <img src="assets/img/img-lienhe3.png">
                            </div>
                            <div class="invalid-feedback"><?=vuilongnhapdiachiemail?></div>
                        </div>
                    </div>
                    <div class="input-contact">
                        <textarea class="form-control" id="noidung" name="noidung" placeholder="Nội dung cần tư vấn..."></textarea>
                        <div class="invalid-feedback"><?=vuilongnhapnoidung?></div>
                    </div>
                    <input type="submit" class="btn btn-primary1" name="submit-contact" value="Gửi thông tin" disabled />
                    <input type="hidden" name="recaptcha_response_contact" id="recaptchaResponseContact">
                </form>
            </div>
            <div class="left-lienhe1">
                <div class="text-ggmap-ct">
                    <p>GOOGLE MAP</p>
                </div>
                <div class="bottom-contact1"><?=htmlspecialchars_decode($optsetting['toado_iframe'])?></div>
            </div>
        </div>
    </div>
</div>