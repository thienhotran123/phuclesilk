<!DOCTYPE html>
<html lang="<?=$config['website']['lang-doc']?>">
<head>
    <?php include TEMPLATE.LAYOUT."head.php"; ?>
    <?php include TEMPLATE.LAYOUT."css.php"; ?>
</head>
<body <?php if($source=='index'){ echo 'class="index"'; } ?>>
<?php if($source=="index") {?>
    <div class="wap_main">
        <style type="text/css">
            .mask { width: 100%; height: 100vh; position: fixed; left: 0; top: 0; z-index: 999999999; overflow: hidden; } 
            .mask span:nth-child(1) { height: 30vh; top: 0; -webkit-transition-delay: .3s; transition-delay: .3s; } 
            .mask span:nth-child(2) { height: 40vh; top: 30vh; -webkit-transition-delay: .5s; transition-delay: .5s; } 
            .mask span:nth-child(3) { height: 30vh; top: 70vh; -webkit-transition-delay: .6s; transition-delay: .6s; } 
            .mask span { width: 100%; position: absolute; right: 0; background: -webkit-linear-gradient(90deg,#fff 0,#fff 100%); background: linear-gradient(90deg,#fff 0,#fff 100%); -webkit-transition: width .9s ease-in-out; transition: width .9s ease-in-out; } 
            .mask.hideg span { width: 0; } 
            .mask.hideg { pointer-events: none; }
            .loadicon { position: fixed; top: 50%; left: 50%; width: 200px; height: 140px; margin: -70px 0 0 -100px; z-index: 110000; } 
            /* loading */
            #loading { position: fixed; top: 0; left: 0; width: 100%; height: 100%; z-index: 999999999999; } 
            #loading.finish { z-index: -9999; } 
            #loading.finish .logo_2 span, #loading.finish .logo_2 img { display: none; } 
            #loading .logo_2 { width: 100%; height: 100%; display: flex; align-items: center; justify-content: center; transition: all ease 0.5s; } 
            #loading .logo_2 span { display: block; border-radius: 50%; border: 3px solid rgba(255, 255, 255, 0.5); -ms-border-radius: 50%; -o-border-radius: 50%; -webkit-border-radius: 50%; border-radius: 50%; box-shadow: 0 0 20px rgba(255, 255, 255, 0.5); -webkit-animation: Ani 2s infinite; animation: Ani 2s infinite; width: 150px; height: 150px; position: absolute; left: 0; right: 0; margin-left: auto; margin-right: auto; z-index: 2; z-index: 9999999999; } 
            #loading .logo_2 img { position: relative; max-width: 150px; z-index: 4; } 
            @-webkit-keyframes Ani { 
                0% { box-shadow:0 0 0 #242424; border:1px solid #242424; -webkit-transform:scale(0); transform:scale(0); } 
            }

            .start-animate { z-index: 999 !important; position: absolute; animation: star linear 1.75s infinite; -moz-animation: star linear 1.75s infinite; -webkit-animation: star linear 1.75s infinite; -o-animation: star linear 1.75s infinite; } 
            @keyframes star { 
                0% { transform: rotate(0) scale(0); } 
                50% { transform: rotate(180deg) scale(1.5); } 
                100% { transform: rotate(360deg) scale(0); } 
            }
        </style>
        <div class="mask">
            <span></span>
            <span></span>
            <span></span>
        </div>
        <div id="loading">
            <div class="logo_2">
                <a href="" class="peShiner">
                    <img onerror="this.src='<?=THUMBS?>/175x126x2/assets/images/noimage.png';" src="<?=THUMBS?>/175x126x2/<?=UPLOAD_PHOTO_L.$logo['photo']?>"/>
                </a>    
            </div>
        </div>
<?php }?>
    <?php
    include TEMPLATE.LAYOUT."seo.php";
    include TEMPLATE.LAYOUT."header.php";
    include TEMPLATE.LAYOUT."banner.php";
    include TEMPLATE.LAYOUT."menu.php";
    include TEMPLATE.LAYOUT."mmenu.php";
    if($source=='index') include TEMPLATE.LAYOUT."slide.php";
    else include TEMPLATE.LAYOUT."breadcrumb.php";
    ?>
    <?php if($source=='index'){ ?>
        <?php include TEMPLATE.$template."_tpl.php"; ?>
    <?php } else { ?>
        <div id="container" class="center w-clear">
            <?php include TEMPLATE.$template."_tpl.php"; ?>
        </div>
    <?php } ?>
    <?php
    include TEMPLATE.LAYOUT."footer.php";
    include TEMPLATE.LAYOUT."support.php";
    include TEMPLATE.LAYOUT."modal.php";
    include TEMPLATE.LAYOUT."js.php";
        // if($deviceType=='mobile') include TEMPLATE.LAYOUT."phone.php";
    ?>
    <div class="progress-wrap cursor-pointer">
        <svg class="progress-circle svg-content" width="100%" height="100%" viewBox="-1 -1 102 102">
            <path d="M50,1 a49,49 0 0,1 0,98 a49,49 0 0,1 0,-98" />
        </svg>
    </div>
</body>
</html>