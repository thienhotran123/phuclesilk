<!-- Giới thiệu -->
<div class="bgr-gioithieu">
	<div class="center-1195">
		<div class="main-gioithieu">
			<div class="div-gioithieu">
				<div class="left-gioithieu wow animate__animated animate__backInLeft">
					<div class="gioithieuve">Giới thiệu về</div>
					<div class="ten-gioithieu"><?=$gioithieu['ten']?></div>
					<div class="mota-gioithieu"><?=htmlspecialchars_decode($gioithieu['mota'])?></div>
					<div class="text-xemthem-gioithieu"> <a <?php if($com=='gioi-thieu') echo 'active'; ?> href="gioi-thieu" title="<?=gioithieu?>">Xem thêm</a></div>
				</div>
				<div class="right-gioithieu wow animate__animated animate__backInRight">
					<div class="img-gioithieu">
						<a  class="text-decoration-none scale-img " href="gioi-thieu">
						<img onerror="this.src='<?=THUMBS?>/614x417x2/assets/images/noimage.png';" src="<?=THUMBS?>/614x417x2/<?=UPLOAD_NEWS_L.$gioithieu['photo']?>" alt="<?=$gioithieu['ten']?>">
						</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<!-- Sản phẩm nổi bật -->
<div class="bgr-sanphamnoibat">
	<div class="center-1195">
		<div class="main-sanphamnoibat wow animate__animated animate__backInUp">
			<div class="text-sanphamnoibat">
				<p>Sản phẩm nổi bật</p>
				<img src="assets/img/gachspnb.png">
			</div>
			<div class="div-sanphamnoibat">
				<?php for($i=0,$count=count($sanphamnb); $i<$count; $i++) { ?>
					<div class="wrap-sanphamnoibat">
						<div class="sanphamnoibat-wrap">
							<div class="img-sanphamnoibat">
								<a class="text-decoration-none scale-img" href="<?=$sanphamnb[$i][$sluglang]?>" title="<?=$sanphamnb[$i]['ten']?>">
								<img onerror="this.src='<?=THUMBS?>/278x286x1/assets/images/noimage.png';" src="<?=THUMBS?>/278x286x2/<?=UPLOAD_PRODUCT_L.$sanphamnb[$i]['photo']?>" alt="<?=$sanphamnb[$i]['ten']?>">
								</a>
							</div>
							<div class="ten-sanphamnoibat">
								<h3>
									<a class="text-decoration-none text-split text-split-2" href="<?=$sanphamnb[$i][$sluglang]?>" title="<?=$sanphamnb[$i]['ten']?>">
									<?=$sanphamnb[$i]['ten']?>
									</a>
								</h3>
							</div>
							<div class="text-giasp">
								<?php if($sanphamnb[$i]['giakm']) {?>
									<p class="text-giasp2"><?=$func->format_money($sanphamnb[$i]['giamoi'])?></p>
								<?php }elseif($sanphamnb[$i]['gia']){?>
									<p class="text-giasp2"><?=$func->format_money($sanphamnb[$i]['gia'])?></p>
								<?php }else {?>
									<p class="text-giasp2">Liên hệ</p>
								<?php }?>
							</div>
						</div>
					</div>
				<?php }?>
			</div>
			<div class="xemthem-sanpham">
				<a href="">Xem thêm sản phẩm</a>						
			</div>
		</div>
	</div>
</div>

<!-- Sản phẩm c1 -->
<div class="bgr-sanphamc1">
	<div class="center-1195">
		<div class="main-sanphamc1 wow animate__animated animate__backInUp">
			<?php for($i=0,$count=count($danhmuc1sanphamnb); $i<$count; $i++) { ?>
				<?php $itemlistsp = $d->rawQuery("select ten$lang as ten, photo, gia, giamoi, tenkhongdauvi, tenkhongdauen, id from #_product where id_list = ? and noibat > 0  and hienthi > 0 order by stt,id desc limit 0,8",array($danhmuc1sanphamnb[$i]['id']));?>
				<div class="div-tenc1-1">
					<h3 class="text-spc1">
						<span class="text-decoration-none text-split text-split-2" href="<?=$danhmuc1sanphamnb[$i][$sluglang]?>" title="<?=$danhmuc1sanphamnb[$i]['ten']?>">
							<?=$danhmuc1sanphamnb[$i]['ten']?> 
						</span>
					</h3>
				</div>
				<div class="div-sanphamnoibat">
					<?php for($j=0,$countn=count($itemlistsp); $j<$countn; $j++) { ?>
						<div class="wrap-sanphamnoibat">
							<div class="sanphamnoibat-wrap">
								<div class="img-sanphamnoibat">
									<a class="text-decoration-none scale-img" href="<?=$itemlistsp[$j][$sluglang]?>" title="<?=$itemlistsp[$j]['ten']?>">
									<img onerror="this.src='<?=THUMBS?>/278x286x1/assets/images/noimage.png';" src="<?=THUMBS?>/278x286x2/<?=UPLOAD_PRODUCT_L.$itemlistsp[$j]['photo']?>" alt="<?=$itemlistsp[$j]['ten']?>">
									</a>
								</div>
								<div class="ten-sanphamnoibat">
									<h3>
										<a class="text-decoration-none text-split text-split-2" href="<?=$itemlistsp[$j][$sluglang]?>" title="<?=$itemlistsp[$j]['ten']?>">
										<?=$itemlistsp[$j]['ten']?>
										</a>
									</h3>
								</div>
								<div class="text-giasp">
									<?php if($itemlistsp[$j]['giakm']) {?>
										<p class="text-giasp2"><?=$func->format_money($itemlistsp[$j]['giamoi'])?></p>
									<?php }elseif($itemlistsp[$j]['gia']){?>
										<p class="text-giasp2"><?=$func->format_money($itemlistsp[$j]['gia'])?></p>
									<?php }else {?>
										<p class="text-giasp2">Liên hệ</p>
									<?php }?>
								</div>
							</div>
						</div>
					<?php }?>
				</div>
				<div class="xemthem-sanpham1">
					<a href="<?=$danhmuc1sanphamnb[$i][$sluglang]?>">Xem thêm sản phẩm</a>						
				</div>		
			<?php } ?>				
		</div>
	</div>								
</div>

<!-- Dịch vụ -->
<div class="bgr-dichvu">
	<div class="center-1195">
		<div class="main-dichvu wow animate__animated animate__backInUp">
			<div class="text-dichvu">
				<p>Dịch vụ</p>
				<img src="assets/img/gachspnb.png">					
			</div>
			<div class="slick-dichvu">
				<?php for($i=0,$count=count($dichvu); $i<$count; $i++) { ?>
					<div>
						<div class="wrap-dichvu">
							<div class="img-dichvu">
								<a class="text-decoration-none scale-img" href="<?=$dichvu[$i][$sluglang]?>" title="<?=$dichvu[$i]['ten']?>">
								<img onerror="this.src='<?=THUMBS?>/570x322x1/assets/images/noimage.png';" src="<?=THUMBS?>/570x322x1/<?=UPLOAD_NEWS_L.$dichvu[$i]['photo']?>" alt="<?=$dichvu[$i]['ten']?>">
								</a>
							</div>
							<div class="ten-dichvu">
								<h3>
									<a class="text-decoration-none text-split text-split-2" href="<?=$dichvu[$i][$sluglang]?>" title="<?=$dichvu[$i]['ten']?>">
									<?=$dichvu[$i]['ten']?>
									</a>
								</h3>
							</div>
						</div>
					</div>
				<?php } ?>					
			</div>
			<div class="xemtatca-dichvu">
				<a href="dich-vu">Xem tất cả</a>
			</div>						
		</div>
	</div>
</div>

<!-- Đối tác -->
<div class="bgr-doitac">
	<div class="center-1195">
		<div class="main-doitac wow animate__animated animate__backInUp">
			<div class="text-doitac">
				<p>Đối tác</p>
			</div>
			<?php if(count($doitac)) { ?>
				<div id="doitac">
					<div class="center d-flex align-items-center justify-content-between">
						<p class="control-carousel prev-carousel prev-doitac transition"><i class="fas fa-chevron-left"></i></p>
						<div class="owl-carousel owl-theme owl-doitac">
							<?php for($i=0,$count=count($doitac); $i < $count; $i++) { ?>
								<div>
									<a href="<?=$doitac[$i]['link']?>" target="_blank" title="<?=$doitac[$i]['ten']?>">
										<img onerror="this.src='<?=THUMBS?>/130x30x1/assets/images/noimage.png';" src="<?=THUMBS?>/130x30x2/<?=UPLOAD_PHOTO_L.$doitac[$i]['photo']?>" alt="<?=$doitac[$i]['ten']?>"/>
									</a>
								</div>
							<?php } ?>
						</div>
						<p class="control-carousel next-carousel next-doitac transition"><i class="fas fa-chevron-right"></i></p>
					</div>
				</div>
			<?php } ?>
		</div>
	</div>
</div>

<!-- Khách hàng -->
<div class="bgr-khachhang">
	<div class="center-650">
		<div class="main-khachhang wow animate__animated animate__backInUp">
			<div class="text-khachhang">
				<p>Khách hàng nói gì về chúng tôi</p>				
			</div>
			<div class="img-khachhang">
				<img src="assets/img/khachhang1.png">
			</div>
			<div class="slick-khachhang">
				<?php for($i=0,$count=count($khachhang); $i<$count; $i++) { ?>
					<div class="wrap-khachhang">
						<div class="mota-khachhang">
							<?=$khachhang[$i]['mota']?>
						</div>
						<div class="ten-khachhang">
							<?=$khachhang[$i]['ten']?>
						</div>
					</div>
				<?php } ?>		
			</div>				
		</div>
	</div>
</div>

<!-- Tin tức và video -->
<div class="bgr-tintuc">
	<div class="center-1195">
		<div class="main-tintuc">
			<div class="text-tintuc-sukien wow animate__animated animate__backInUp">
				<p>TIN TỨC</p>
			</div>
			<div class="text-tintuc-sukien wow animate__animated animate__backInDown">
				<p>SỰ KIỆN</p>
			</div>
			<div class="div-tintuc">
				<div class="left-tintuc wow animate__animated animate__backInLeft">
					<div class="video1">
						<?php for($i=0,$count=count($videonb); $i<$count; $i++) { ?>
							<div class="wrapvideo">
								<div href="<?=$videonb[$i][$sluglang]?>"data-fancybox="video" data-src="<?=$videonb[$i]['link_video']?>" title="<?=$videonb[$i]['ten']?>">
								<div class="pic-video scale-img">
									<img onerror="this.src='<?=THUMBS?>/597x315x1/assets/images/noimage.png';" src="<?=THUMBS?>/597x315x2/<?=UPLOAD_PHOTO_L.$videonb[$i]['photo']?>" alt="<?=$videonb[$i]['ten']?>">
									</div>
								</div>
							</div>
						<?php } ?>	
					</div>
				</div>
				<div class="right-tintuc wow animate__animated animate__backInRight">
					<div class="rlght-tintuc-1">
						<?php for($i=0,$count=count($tintucnb); $i<$count; $i++) { ?>
							<div class="wraptintuc-1 <?= ($i % 2 == 0) ? 'border-bottom1' : 'border-right1'?>">
								<div class="ten-tintuc-1">
									<h3>
										<a class="text-decoration-none text-split text-split-2" href="<?=$tintucnb[$i][$sluglang]?>" title="<?=$tintucnb[$i]['ten']?>">
										<?=$tintucnb[$i]['ten']?>
										</a>
									</h3>
								</div>
							</div>
						<?php } ?>
					</div>
					<div class="xemtaca-tintuc">
						<a href="tin-tuc">Xem tất cả</a>
					</div>	
				</div>		
			</div>
		</div>
	</div>
</div>