<div id="footer">
    <div class="footer-top">
        <div class="center d-flex flex-wrap align-items-start justify-content-between">
            <div class="footer-4 wow animate__animated animate__backInLeft">
                <?php if($logofooter) {?>
                    <div class="logofooter sssss d-flex align-items-center">
                        <a href=""><img onerror="this.src='<?=THUMBS?>/112x105x2/assets/images/noimage.png';" src="<?=THUMBS?>/112x105x2/<?=UPLOAD_PHOTO_L.$logofooter['photo']?>"/></a>
                    </div>
                <?php }?>
            </div>
            <div class="footer-1 wow animate__animated animate__backInLeft">
                <div class="footer-tit"><?=$footer['ten']?></div>
                <div class="footer-content"><?=htmlspecialchars_decode($footer['noidung'])?></div>
            </div>
            <div class="footer-2 wow animate__animated animate__backInRight">
                <?php if(isset($chinhsach)){ ?>
                    <div class="footer-tit"><?=chinhsach?></div>
                    <ul class="footer-list">
                        <?php for($i=0,$count=count($chinhsach); $i < $count; $i++) { ?>
                            <li><a class="text-decoration-none" href="<?=$chinhsach[$i][$sluglang]?>" title="<?=$chinhsach[$i]['ten']?>"><?=$chinhsach[$i]['ten']?></a></li>
                        <?php } ?>
                    </ul>
                <?php } ?>
            </div>
            <div class="footer-3 wow animate__animated animate__backInRight">
                <div class="footer-tit">Nhận báo giá</div>
                    <p class="slogan-newsletter">Nhận thông báo về các sự kiện và ưu đãi
                        hấp dẫn sớm nhất từ chúng tôi</p>
                    <form class="form-newsletter validation-newsletter" novalidate method="post" action="" enctype="multipart/form-data">
                        <div class="newsletter-input">
                            <input type="email" class="form-control" id="email-newsletter" name="email-newsletter" placeholder="Email" required />
                        </div>
                        <div class="newsletter-button">
                            <input type="submit" name="submit-newsletter" value="<?=gui?>" disabled>
                            <input type="hidden" name="recaptcha_response_newsletter" id="recaptchaResponseNewsletter">
                        </div>
                </form>
            </div>
        </div>
    </div>
    <div class="footer-bottom">
        <div class="center d-flex flex-wrap align-items-center justify-content-center">
            <p class="copyright">Copyright ©2022 <?=$setting["ten$lang"]?>. Design by <a href="https://vinasoftware.com.vn/">Vina Software (VNS)</a></p>
        </div>
    </div>
</div>