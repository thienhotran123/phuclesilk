<div id="menu">
    <div class="center d-flex align-items-center justify-content-between">
        <div class="div-logo">
            <?php if($logo) {?>
				<div class="logo sss d-flex align-items-center">
					<a href=""><img onerror="this.src='<?=THUMBS?>/71x66x2/assets/images/noimage.png';" src="<?=THUMBS?>/71x66x2/<?=UPLOAD_PHOTO_L.$logo['photo']?>"/></a>
				</div>
			<?php }?>                                                               
        </div>
        <div class="div-menu">
            <ul class="d-flex  justify-content-start">
                <li>
                    <a class="transition <?php if($com=='' || $com=='index') echo 'active'; ?>" href="" title="<?=trangchu?>"><?=trangchu?></a>
                </li>
                <li>
                    <a class="transition <?php if($com=='gioi-thieu') echo 'active'; ?>" href="gioi-thieu" title="<?=gioithieu?>"><?=gioithieu?></a>
                </li>
                <li>
                    <a class="transition <?php if($com=='san-pham') echo 'active'; ?>" href="san-pham" title="<?=sanpham?>"><?=sanpham?></a>
                    <?php if(count($splistmenu)) { ?>
                        <ul>
                            <?php for($i=0;$i<count($splistmenu); $i++) {
                                $spcatmenu = $d->rawQuery("select ten$lang as ten, tenkhongdauvi, tenkhongdauen, id from #_product_cat where id_list = ? and hienthi > 0 order by stt,id desc",array($splistmenu[$i]['id'])); ?>
                                <li>
                                    <a class="transition" title="<?=$splistmenu[$i]['ten']?>" href="<?=$splistmenu[$i][$sluglang]?>"><span><?=$splistmenu[$i]['ten']?></span></a>
                                    <?php if(count($spcatmenu)>0) { ?>
                                        <ul>
                                            <?php for($j=0;$j<count($spcatmenu);$j++) {
                                                $spitemmenu = $d->rawQuery("select ten$lang as ten, tenkhongdauvi, tenkhongdauen, id from #_product_item where id_cat = ? and hienthi > 0 order by stt,id desc",array($spcatmenu[$j]['id'])); ?>
                                                <li>
                                                    <a class="transition" title="<?=$spcatmenu[$j]['ten']?>" href="<?=$spcatmenu[$j][$sluglang]?>"><span><?=$spcatmenu[$j]['ten']?></span></a>
                                                    <?php if(count($spitemmenu)) { ?>
                                                        <ul>
                                                            <?php for($u=0;$u<count($spitemmenu);$u++) {
                                                                $spsubmenu = $d->rawQuery("select ten$lang as ten, tenkhongdauvi, tenkhongdauen, id from #_product_sub where id_item = ? and hienthi > 0 order by stt,id desc",array($spitemmenu[$u]['id'])); ?>
                                                                <li>
                                                                    <a class="transition" title="<?=$spitemmenu[$u]['ten']?>" href="<?=$spitemmenu[$u][$sluglang]?>"><span><?=$spitemmenu[$u]['ten']?></span></a>
                                                                    <?php if(count($spsubmenu)) { ?>
                                                                        <ul>
                                                                            <?php for($s=0;$s<count($spsubmenu);$s++) { ?>
                                                                                <li>
                                                                                    <a class="transition" title="<?=$spsubmenu[$s]['ten']?>" href="<?=$spsubmenu[$s][$sluglang]?>"><span><?=$spsubmenu[$s]['ten']?></span></a>
                                                                                </li>
                                                                            <?php } ?>
                                                                        </ul>
                                                                    <?php } ?>
                                                                </li>
                                                            <?php } ?>
                                                        </ul>
                                                    <?php } ?>
                                                </li>
                                            <?php } ?>
                                        </ul>
                                    <?php } ?>
                                </li>
                            <?php } ?>
                        </ul>
                    <?php } ?>
                </li>
                <li>
                    <a class="transition <?php if($com=='tin-tuc') echo 'active'; ?>" href="tin-tuc" title="<?=tintuc?>"><?=tintuc?></a>
                </li>
                <li>
                    <a class="transition <?php if($com=='dich-vu') echo 'active'; ?>" href="dich-vu" title="<?=dichvu?>"><?=dichvu?></a>
                </li>
                <li>
                    <a class="transition <?php if($com=='lien-he') echo 'active'; ?>" href="lien-he" title="<?=lienhe?>"><?=lienhe?></a>
                </li>
            </ul>
        </div>
        <div class="div-timkiem">
            <div class="div-timkiem-ggdich1">
                <div class="search w-clear">
                    <input type="text" id="keyword" placeholder="Tìm kiếm" onkeypress="doEnter(event,'keyword');"/>
                    <p onclick="onSearch('keyword');"><i class="fas fa-search"></i></p>
                    </div>                                                             
                </div>
                <div 
                    id="google_language_translator" class="google_language_translator dp-none">
                </div>
            </div>                                                                   
        </div>                                                             
    </div>
</div>