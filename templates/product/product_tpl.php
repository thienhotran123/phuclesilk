<div class="title-main">
    <h1><?=(@$title_cat!='')?$title_cat:@$title_crumb?></h1>
    <p><?=$slogan['ten']?></p>
</div>
<div class="w-clear">
    <?php if(isset($product) && count($product) > 0) { ?>
        <div class="div-sanphamnoibat">
            <?php for($i=0,$count=count($product); $i<$count; $i++) { ?>
                <div class="wrap-sanphamnoibat">
                    <div class="sanphamnoibat-wrap">
                        <div class="img-sanphamnoibat">
                            <a class="text-decoration-none scale-img" href="<?=$product[$i][$sluglang]?>" title="<?=$product[$i]['ten']?>">
                            <img onerror="this.src='<?=THUMBS?>/278x286x1/assets/images/noimage.png';" src="<?=THUMBS?>/278x286x2/<?=UPLOAD_PRODUCT_L.$product[$i]['photo']?>" alt="<?=$product[$i]['ten']?>">
                            </a>
                        </div>
                        <div class="ten-sanphamnoibat">
                            <h3>
                                <a class="text-decoration-none text-split text-split-2" href="<?=$product[$i][$sluglang]?>" title="<?=$product[$i]['ten']?>">
                                <?=$product[$i]['ten']?>
                                </a>
                            </h3>
                        </div>
                        <div class="text-giasp">
                            <?php if($product[$i]['giakm']) {?>
                                <p class="text-giasp2"><?=$func->format_money($product[$i]['giamoi'])?></p>
                            <?php }elseif($product[$i]['gia']){?>
                                <p class="text-giasp2"><?=$func->format_money($product[$i]['gia'])?></p>
                            <?php }else {?>
                                <p class="text-giasp2">Liên hệ</p>
                            <?php }?>
                        </div>
                    </div>
                </div>
            <?php }?>
        </div>
    <?php } else { ?>
        <div class="alert alert-danger" role="alert">
            <strong><?=khongtimthayketqua?></strong>
        </div>
    <?php } ?>
    <div class="clear"></div>
    <div class="pagination-home mgt-25"><?=(isset($paging) && $paging != '') ? $paging : ''?></div>
</div>